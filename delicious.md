<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#sec-1">1. The main goal</a></li>
<li><a href="#sec-2">2. The main algorithm</a></li>
<li><a href="#sec-3">3. About previous version</a></li>
<li><a href="#sec-4">4. About API</a></li>
<li><a href="#sec-5">5. Dactyl writing-plugin documentation</a></li>
<li><a href="#sec-6">6. Initializing our plugin</a></li>
</ul>
</div>
</div>

# The main goal

is useful working with delicious.com service. What do I mean:

-   Add new posts with tags

-   Get posts list

    -   By the date

    -   By the tags

        -   get tag list

    -   All

-   Use pentadactyl's tools to show posts list

# The main algorithm

-   switch from five methods

    1.  *add*

    2.  *tag*

    3.  *tags*

    4.  *period*

    5.  *all*

-   send request to **rootURL**

-   print some result to **dactyl.echo** or show interactive list to choose link (?)

# About previous version

I use simple one [delicious.js](http://code.google.com/p/dactyl/issues/detail?id%3D514&q%3Dproject%253ADactyl%252Cpentadactyl%2520type%253Aplugin&colspec%3DID%2520Type%2520Status%2520Priority%2520Stars%2520Owner%2520Summary) as a base of my plugin.
About delicious.js:

    // Pentadactyl Plugin: 'Delicious'
    // Last Change: 19-Sept-2013
    // License: MIT
    // Usage: Use :delicious <add|tag|tags|all|period> ["description in quotes"] <tags delimited by comma>
    //
    // Old info:
    //// Maintained by: Egon Hyszczak <gone404@gmail.com>
    //// Changes made: Added private bookmarks (pvt) and Twitter functionality (for:@twitter).
    //// Modified for use with Pentadactyl.
    //// Previous Maintainer: Travis Jeffery <travisjeffery@gmail.com>

# About [API](http://api.del.icio.us/developers#title1)

Original script has used only one API method: add.
Our purpose is using at least five methods:

-   [add](https://api.del.icio.us/v1/posts/add?)

-   [get with tag](https://api.del.icio.us/v1/posts/get?tag%3D{TAG})

-   [get tag list](https://api.del.icio.us/v1/tags/get)

-   [get all](https://api.del.icio.us/v1/posts/all)

-   [get all by date](https://api.del.icio.us/v1/posts/all?fromdt%3D{fromDate}&todt%3D{toDate})

# Dactyl writing-plugin documentation

Dactyl documentation is one of the worst documentation ever!

# Initializing our plugin

Likely we have to add our command to group.commands

    group.commands.add(['delicious'], "Save page as a bookmark on Delicious",
                       function(method, name, tags) {

We have one root url, for wich we will add additional parameters

    rootUrl = "https://api.del.icio.us/v1/"

And our additional parameters for API methods:

    addMethod = "posts/add?",
    getMethod = "posts/get",
    allMethod = "posts/all",
    tagListMethod = "tags/get"

In JavaScript is a good practice to define variables in one place into the function

    var <<root_url>>,
        <<API_methods>>;
