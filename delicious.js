
// Pentadactyl Plugin: 'Delicious'
// Last Change: 19-Sept-2013
// License: MIT
// Usage: Use :delicious <add|tag|tags|all> ["description in quotes"] <tags delimited by comma>
//
// Old info:
//// Maintained by: Egon Hyszczak <gone404@gmail.com>
//// Changes made: Added private bookmarks (pvt) and Twitter functionality (for:@twitter).
//// Modified for use with Pentadactyl.
//// Previous Maintainer: Travis Jeffery <travisjeffery@gmail.com>
"use strict";
group.commands.add(['delicious'], "Save page as a bookmark on Delicious",
                   function(method, name, tags) {
    var rootUrl = "https://api.del.icio.us/v1/",
        addMethod = "posts/add?",
        getMethod = "posts/get",
        allMethod = "posts/all",
        tagListMethod = "tags/get";
});
